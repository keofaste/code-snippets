class CustomBaseForm:
    """
    Available field frontend types:
    - choice
    - date
    - multiple_choice
    - number
    - radio
    - range
    - text
    - toggle
    """

    class FilterMeta:
        field_frontend_types = {}
        field_radio_group = {}

    def api_errors(self):
        errors_list = []
        for field in self.errors:
            errors = [', '.join(data.messages) for data in self.errors[field].data]

            for error in errors:
                error_data = {
                    'code': 201,
                    'message': error
                }
                if field != '__all__':
                    error_data['field'] = field

                errors_list.append(error_data)

        return errors_list

    def need_filter(self):
        for value in self.cleaned_data.values():
            if value:
                return True

        return False

    def get_filtered_data(self):
        return {
            'data': []
        }

    def get_filters_initial_values(self):
        filter_values = {}
        for field in self.fields:
            field_frontend_type = self.FilterMeta.field_frontend_types.get(field, 'text')
            values_list = []

            if field_frontend_type in ['multiple_choice', 'choice']:
                for value in self.fields[field].choices:
                    values_list.append({
                        'id': value[0],
                        'name': value[1],
                        'available': True
                    })
                filter_values[field] = values_list
            elif field_frontend_type in ['text', 'toggle', 'range']:
                filter_values[field] = {'available': True}
            elif field_frontend_type == 'radio':
                field_radio_group = self.FilterMeta.field_radio_group.get(field, field)
                if field_radio_group in filter_values:
                    filter_values[field_radio_group].append({
                        'id': field,
                        'name': str(self.fields[field].label),
                        'available': True
                    })
                else:
                    filter_values[field_radio_group] = [{
                        'id': field,
                        'name': str(self.fields[field].label),
                        'available': True
                    }]
            elif field_frontend_type == 'number':
                filter_values[field] = {
                    'available': True,
                    'min_value': self.fields[field].min_value,
                    'max_value': self.fields[field].max_value
                }
            elif field_frontend_type == 'date':

                filter_values[field] = self.fields[field].get_initial_value()

        return filter_values

class CustomForm(forms.Form, CustomBaseForm):
    pass