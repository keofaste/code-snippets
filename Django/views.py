@api_decorator()
@custom_permission_required('Ipam.StaticRoute.view')
def static_route_filter(request):
    """
    ��������� ������ StaticRoute. �������������� ���������� ���������� ��������.
    ��� ��������� ���������� ������� ��� ������� ���� �� ��������, ����� `data`` ����� ������ ��������.


    **url**: ``/ipam/static_route/api/filter/``

    **url template**: ``Ipam:StaticRoute:Api:Filter``

    **��������� �������:**

    * ``filter['prefix']`` *str* ip network
    * ``filter['domain']`` *list of int* domain_id
    * ``filter['next_hop']`` *str* ip network
    * ``filter['client']`` *list of int* client_id
    * ``filter['history']`` *bool*
    * ``filter['history_date']`` *str*
        * ����� �������� ������ ��� ``history=true``
        * ��������� �� ���� ��� ���� � ������� iso8601 ����� `` - ``

    **�������������� ���������:**

    * ``meta['exclude_filters_data']`` *bool* ���� true, ���� ``filter`` �� ����� �������� � �����

    **��� �������� ����������:**

    * ���� ``data`` �������� ������ �������� StaticRoute.
    * ���� ``filter`` �������� ������� � ���������� ���������� ����� ��� ������.


    **������ ������������ ������:**

    ������ ������������ � json-�������

    >>>    {
                'filter': {
                    'history_date': {'available': bool, 'set_start': None or date, 'set_end': None or date},
                    'prefix': {'available': bool},
                    'domain': [{'id': int, 'name': str, 'available': bool}, ...]
                    'next_hop': {'available': bool},
                    'client': [{'id': int, 'name': str, 'available': bool}, ...]
                    'history': {'available': bool}
                    },
                'data': [
                    {
                        'id': int,
                        'prefix': str,
                        'next_hop': [str, str, ...],
                        'client': str,
                        'client_id': int,
                        'vc_domain': str,
                        'vc_domain_id': int,
                        'properties': {},
                        'actions': [str, str, ...]
                    },
                    ...
                ],
                'meta': {
                    'ver': 1
                }
            }
    """
    form = FilterForm(request.api_data.get('filter', {}))

    if not form.is_valid():
        return JsonResponse({
            'error': form.api_errors(),
            'meta': {
                'ver': 1
            }
        }, status=444)

    try:
        data = form.get_filtered_data()
    except Exception as e:
        return JsonResponse({
            'error': [{
                'code': 300,
                'message': _('Error on filter: {}'.format(e)),
            }],
            'meta': {
                'ver': 1,
            }
        }, status=444)

    response_data = {
        'data': data,
        'meta': {
            'ver': 1
        }
    }

    exclude_filters_data = request.api_data['meta'].get('exclude_filters_data', False)
    if not exclude_filters_data:
        response_data.update({
            'filter': form.get_filters_initial_values()
        })

    return JsonResponse(response_data, status=200)