class FilterForm(CustomForm):
    class FilterMeta:
        field_frontend_types = {
            'prefix': 'text',
            'domain': 'multiple_choice',
            'next_hop': 'text',
            'client': 'multiple_choice',
            'history': 'toggle',
            'history_date': 'date',
        }

    history_date = DateRangeField(
        required=False,
        label=_('Date')
    )

    prefix = forms.CharField(required=False, label=_('Prefix'))

    domain = CustomModelMultipleChoiceField(
        queryset=VcDomain.objects.none(),
        required=False,
        label=_('Domain')
    )

    next_hop = forms.CharField(required=False, label=_('Destination ip'))

    client = CustomModelMultipleChoiceField(
        queryset=Profile.objects.none(),
        required=False,
        label=_('Client')
    )

    history = forms.BooleanField(required=False, label=_('History'))

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)

        self.fields['domain'].queryset = VcDomain.objects.all()
        self.fields['client'].queryset = Profile.current_objects().filter(billing_id__isnull=False)

    def need_filter(self):
        required_fields = ['prefix', 'domain', 'vlan', 'next_hop', 'client', 'status', 'history_date']

        for key, value in self.cleaned_data.items():
            if key in required_fields and value:
                return True

        return False

    def clean_prefix(self):
        value = self.cleaned_data['prefix']
        if not value:
            return None

        try:
            value = ipaddress.ip_network(value)
        except:
            raise forms.ValidationError(_('Invalid prefix'))

        return value

    def clean_next_hop(self):
        value = self.cleaned_data['next_hop']
        if not value:
            return None

        try:
            value = value.split(',')
            value = [ipaddress.ip_network(x) for x in value if x is not None]
        except:
            raise forms.ValidationError(_('Invalid next_hop'))

        return value

    def get_filtered_data(self):

        if not self.need_filter():
            return []

	    data = foo(**self.cleaned_data)
        return data

    def get_filters_initial_values(self):
        filter_values = super(FilterForm, self).get_filters_initial_values()

        filter_values['history_date']['available'] = self.cleaned_data['history']

        return filter_values